/*
 * nfc_task.h
 *
 *  Created on: 11 ���. 2018 �.
 *      Author: Y.BAZYK
 */

#ifndef APPLICATION_SOURCE_NFC_TASK_H_
#define APPLICATION_SOURCE_NFC_TASK_H_

void nfc_create_task(void);

uint8_t capability_container[] = {
    /* NDEF Tag Application name */
    0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01,
    /* Capability Container ID */
    0xE1, 0x03, /* NDEF Id */
    0x00, 0x0F, /* CCLEN */
    0x20,       /* Map version 2.0 */
    0x00, 0xF9, /* MLe */
    0x00, 0xF6, /* MLc */

    0x04,       /* Tag, File control TLV (4 - NDEF) */
    0x06,       /* Length, File control TLV (6 - bytes of data for tag */
    0xE1, 0x04, /* File Id */
    0x0B, 0xDF, /* Max file size */
    0x00,       /* Read access */
    0x00,       /* Write access */
};

uint8_t ndef_file_data[] = {
    0xE1, 0x04, /* NDEF file id */
    0x00, 0x13, /* NLEN: NDEF length */
    0xD1,       /* MB (Message begin = 1),
                 ME (Message end = 1),
                 CF (Chunk flag = 0),
                 SR (Short Record = 1),
                 IL (ID Length = 0),
                 TNF (Well-known type = 001) */
    0x01, 0x0F, /* Type length = 0x01, Payload length = 0x0F */
    0x54,       /* Type = T (text) */
    0x02,       /* Status byte = utf-8, two byte language code */
    0x65, 0x6E  /* Language code - English */
};

#define NFC_DEFAULT_MSG         {                                       \
/* NDEF Tag Application name */                                         \
0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01,                               \
/* Capability Container ID */                                           \
0xE1, 0x03, /* NDEF Id */                                               \
0x00, 0x0F, /* CCLEN */                                                 \
0x20,       /* Map version 2.0 */                                       \
0x00, 0xF9, /* MLe */                                                   \
0x00, 0xF6, /* MLc */                                                   \
0x04,       /* Tag, File control TLV (4 - NDEF) */                      \
0x06,       /* Length, File control TLV (6 - bytes of data for tag */   \
0xE1, 0x04, /* File Id */                                               \
0x0B, 0xDF, /* Max file size */                                         \
0x00,       /* Read access */                                           \
0x00,       /* Write access */                                          \
                                                                        \
0xE1, 0x04, /* NDEF file id */                                          \
0x00, 0x13, /* NLEN: NDEF length */                                     \
0xD1,       /* MB (Message begin = 1),
               ME (Message end = 1),
               CF (Chunk flag = 0),
               SR (Short Record = 1),
               IL (ID Length = 0),
               TNF (Well-known type = 001) */                           \
0x01, 0x0F, /* Type length = 0x01, Payload length = 0x0F */             \
0x54,       /* Type = T (text) */                                       \
0x02,       /* Status byte = utf-8, two byte language code */           \
0x65, 0x6E  /* Language code - English */                               \
}

#endif /* APPLICATION_SOURCE_NFC_TASK_H_ */
