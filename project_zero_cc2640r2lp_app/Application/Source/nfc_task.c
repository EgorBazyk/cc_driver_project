/*
 * nfc_task.c
 *
 *  Created on: 11 ���. 2018 �.
 *      Author: Y.BAZYK
 */

/*********************************************************************
 * INCLUDES
 */
#include <string.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/BIOS.h>

#include <ti/drivers/PIN.h>
#include <xdc/runtime/Log.h>

#include "Board.h"
#include "Application/Drivers/I2C_for_nfc/i2c_for_nfc.h"
#include "Application/Drivers/nfc/nfc.h"
#include "Application/Source/nfc_task.h"

/*********************************************************************
 * CONSTANTS
 */
#define NFC_TASK_STACK_SIZE     ((uint16_t)(800))
#define NFC_TASK_PRIORITY       ((uint8_t)(1))

/* event id */
#define NFC_EVENT               Event_Id_28

/*********************************************************************
 * EXTERN VARIABLES
 */
extern I2C_Handle nfc_i2c_handle;

/*********************************************************************
 * LOCAL VARIABLES
 */

/* Pin configuration */
PIN_State nfc_into_state;

PIN_Config nfc_config_table[] = {
    CC2640R2_LAUNCHXL_NFC_INTO | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_DIS | PIN_HYSTERESIS,
    PIN_TERMINATE
};

/* Task configuration */
Task_Struct nfc_task_struct;
Char nfc_task_stack[NFC_TASK_STACK_SIZE];

/* Event configuration */
Event_Handle nfc_evt_handle;
Event_Params nfc_evt_params;
Event_Struct nfc_evt_struct;

volatile uint32_t nfc_event;

MESSAGE_DATA message_ndef;
uint8_t mac_buffer[12];

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void nfc_task_function(UArg a0, UArg a1);
static void nfc_task_init(void);
static void get_mac_address(uint8_t *mac_buf);

void nfc_interrupt(PIN_Handle handle, PIN_Id pin)
{
    /* disable interrupt */
    PIN_setInterrupt(&nfc_into_state, Board_NFC_INTO | PIN_IRQ_DIS);

    /* post event */
    Event_post(nfc_evt_handle, NFC_EVENT);
}

void nfc_create_task(void)
{
  Task_Params nfc_task_params;

  // Configure task
  Task_Params_init(&nfc_task_params);
  nfc_task_params.stack = nfc_task_stack;
  nfc_task_params.stackSize = NFC_TASK_STACK_SIZE;
  nfc_task_params.priority = NFC_TASK_PRIORITY;

  Task_construct(&nfc_task_struct, nfc_task_function, &nfc_task_params, NULL);

  //Event configuration
  Event_Params_init(&nfc_evt_params);
  Event_construct(&nfc_evt_struct, &nfc_evt_params);

  nfc_evt_handle = Event_handle(&nfc_evt_struct);
}

static void nfc_task_init(void)
{
    /* nfc init */
    nfc_i2c_init();

    /* nfc INTO pin init */
    if (!PIN_open(&nfc_into_state, nfc_config_table)) {
        Log_error0("Error INTO pin open");
    }

    /* register interrupt callback */
    PIN_registerIntCb(&nfc_into_state, nfc_interrupt);

    /* set interrupt on pin Board_NFC_INTO, but disable default */
    PIN_setInterrupt(&nfc_into_state, Board_NFC_INTO);
}

static void nfc_task_function(UArg a0, UArg a1)
{
    uint16_t nfc_register;
    MESSAGE_DATA rx_message_ndef;
    nfc_task_init();

    /* reset nfc and wait */
    write_nfc_register(nfc_i2c_handle, CONTROL_REG, SW_RESET);
    Task_sleep(20 * (1000 / Clock_tickPeriod));

    /* init ndef data */
    get_mac_address(mac_buffer);
    uint16_t cc_size = sizeof(capability_container);
    uint16_t ndef_size = sizeof(ndef_file_data);
    uint16_t mac_size = sizeof(mac_buffer);

    message_ndef.data_addr = RF430_START_ADR;
    memcpy(message_ndef.message, capability_container, cc_size);
    memcpy(message_ndef.message + cc_size, ndef_file_data, ndef_size);
    memcpy(message_ndef.message + cc_size + ndef_size, mac_buffer, mac_size);

    for( ; ; ) {
        /* clear CONTROL_REG */
        write_nfc_register(nfc_i2c_handle, CONTROL_REG, 0x00);

        //TODO INTO interrupt handler
        read_nfc_register(nfc_i2c_handle, INTERRUPT_FLG_REG, &nfc_register, sizeof(nfc_register));
        if (nfc_register & EOR_INT_FLG) {
            Log_info0("NFC data was read");
        } else if (nfc_register & EOW_INT_FLG) {
            read_nfc_data(nfc_i2c_handle, &rx_message_ndef);

        }

        /* clear interrupt flag */
        write_nfc_register(nfc_i2c_handle, INTERRUPT_FLG_REG, 0xFF);

        /* write default ndef */
        write_nfc_data(nfc_i2c_handle, &message_ndef);

        /* enable interrupts */
        write_nfc_register(nfc_i2c_handle, INTERRUPT_ENB_REG, EOR_ENB | EOW_ENB);

        /* enable global interrupts, interrupt output, rf interface */
        write_nfc_register(nfc_i2c_handle, CONTROL_REG, RF_ENABLE | INT_ENABLE | INTO_HIGH | INTO_DRIVE);

        /* clear interrupt */
        PIN_clrPendInterrupt(&nfc_into_state, Board_NFC_INTO);

        /* enable INTO interrupt */
        PIN_setInterrupt(&nfc_into_state, Board_NFC_INTO | PIN_IRQ_NEGEDGE);

        /* pend event */
        nfc_event = Event_pend(nfc_evt_handle, Event_Id_NONE, NFC_EVENT, BIOS_WAIT_FOREVER);
    }
}

static void get_mac_address(uint8_t *mac_buf)
{
    //TODO get mac - address of the device
}
