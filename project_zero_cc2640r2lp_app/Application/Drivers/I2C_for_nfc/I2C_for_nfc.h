/*
 * I2C_for_nfc.h
 *
 *  Created on: 11 ���. 2018 �.
 *      Author: Y.BAZYK
 */

#ifndef APPLICATION_DRIVERS_I2C_FOR_NFC_I2C_FOR_NFC_H_
#define APPLICATION_DRIVERS_I2C_FOR_NFC_I2C_FOR_NFC_H_

#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>

#include "Application/Drivers/nfc/nfc.h"

void nfc_i2c_init(void);
void write_nfc_register(I2C_Handle nfc_i2c_handle, uint16_t reg_addr, uint16_t value);
void read_nfc_register(I2C_Handle nfc_i2c_handle, uint16_t reg_addr, uint8_t *data, size_t length);
void write_nfc_data(I2C_Handle nfc_i2c_handle, const MESSAGE_DATA *data);
void read_nfc_data(I2C_Handle nfc_i2c_handle, MESSAGE_DATA *data);

#endif /* APPLICATION_DRIVERS_I2C_FOR_NFC_I2C_FOR_NFC_H_ */
