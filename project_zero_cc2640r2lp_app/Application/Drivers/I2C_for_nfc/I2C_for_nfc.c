/*
 * I2C_for_nfc.c
 *
 *  Created on: 11 ���. 2018 �.
 *      Author: Y.BAZYK
 */
#include <xdc/runtime/Log.h>

#include <string.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>

#include "stdint.h"
#include "Board.h"

#include "Application/Drivers/I2C_for_nfc/i2c_for_nfc.h"
#include "Application/Drivers/nfc/nfc.h"

I2C_Handle nfc_i2c_handle;
I2C_Params nfc_i2c_params;

void nfc_i2c_init(void)
{
    I2C_init();

    // Configure I2C parameters.
    I2C_Params_init(&nfc_i2c_params);
    nfc_i2c_params.bitRate = I2C_400kHz;
    nfc_i2c_handle = I2C_open(Board_I2C0, &nfc_i2c_params);
    if (nfc_i2c_handle == NULL) {
        Log_error0("Error initialization I2C");
    } else {
        Log_info0("I2C initialized!");
    }
}

void write_nfc_register(I2C_Handle nfc_i2c_handle, uint16_t reg_addr, uint16_t value)
{
    uint8_t tx_buf[4];
    I2C_Transaction nfc_i2c_trans;

    nfc_i2c_trans.slaveAddress = BOARD_RF430CL330_ADDR;

    nfc_i2c_trans.writeBuf = tx_buf;
    nfc_i2c_trans.writeCount = 4;
    nfc_i2c_trans.readCount = 0;

    tx_buf[0] = reg_addr >> 8;
    tx_buf[1] = reg_addr & 0xFF;
    tx_buf[2] = value & 0xFF;
    tx_buf[3] = value >> 8;

    if (!I2C_transfer(nfc_i2c_handle, &nfc_i2c_trans)) {
        Log_error0("Bad I2C transfer write command");
    }
}

void read_nfc_register(I2C_Handle nfc_i2c_handle, uint16_t reg_addr, uint8_t *data, size_t length)
{
    uint8_t tx_buf[2];
    I2C_Transaction nfc_i2c_trans;

    nfc_i2c_trans.slaveAddress = BOARD_RF430CL330_ADDR;

    nfc_i2c_trans.writeBuf = tx_buf;
    nfc_i2c_trans.writeCount = 2;
    nfc_i2c_trans.readBuf = data;
    nfc_i2c_trans.readCount = length;

    tx_buf[0] = reg_addr >> 8;
    tx_buf[1] = reg_addr & 0xFF;

    if (!I2C_transfer(nfc_i2c_handle, &nfc_i2c_trans)) {
        Log_error0("Bad I2C transfer read command");
    }
}

void write_nfc_data(I2C_Handle nfc_i2c_handle, const MESSAGE_DATA *data)
{
    I2C_Transaction nfc_i2c_trans;
    nfc_i2c_trans.slaveAddress = BOARD_RF430CL330_ADDR;

    nfc_i2c_trans.writeBuf = (uint8_t *)data;
    nfc_i2c_trans.writeCount = sizeof(MESSAGE_DATA);
    nfc_i2c_trans.readCount = 0;

    if (!I2C_transfer(nfc_i2c_handle, &nfc_i2c_trans)) {
        Log_error0("Bad I2C transfer write data");
    }
}

void read_nfc_data(I2C_Handle nfc_i2c_handle, MESSAGE_DATA *data)
{
    uint8_t tx_buf[2];
    I2C_Transaction nfc_i2c_trans;

    nfc_i2c_trans.slaveAddress = BOARD_RF430CL330_ADDR;

    nfc_i2c_trans.writeBuf = tx_buf;
    nfc_i2c_trans.writeCount = 2;
    nfc_i2c_trans.readBuf = (uint8_t *)data;
    nfc_i2c_trans.readCount = sizeof(MESSAGE_DATA);

    tx_buf[0] = 0x00;
    tx_buf[1] = 0x00;

    if (!I2C_transfer(nfc_i2c_handle, &nfc_i2c_trans)) {
        Log_error0("Bad I2C transfer read data");
    }
}
