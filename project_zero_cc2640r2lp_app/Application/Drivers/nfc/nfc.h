/*
 * nfc.h
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: Y.BAZYK
 */

#ifndef APPLICATION_DRIVERS_NFC_NFC_H_
#define APPLICATION_DRIVERS_NFC_NFC_H_

/* I2C address */
#define BOARD_RF430CL330_ADDR ((uint8_t)(0x28))

/* User address register map */
#define CONTROL_REG         ((uint16_t)(0xFFFE))
#define STATUS_REG          ((uint16_t)(0xFFFC))
#define INTERRUPT_ENB_REG   ((uint16_t)(0xFFFA))
#define INTERRUPT_FLG_REG   ((uint16_t)(0xFFF8))
#define CRC_RESULT          ((uint16_t)(0xFFF6))
#define CRC_LENGTH          ((uint16_t)(0xFFF4))
#define CRC_START_ADDR_REG  ((uint16_t)(0xFFF2))
#define WD_REG              ((uint16_t)(0xFFF0))
#define VERSION_REG         ((uint16_t)(0xFFEE))

/* CONTROL_REG bits */
#define SW_RESET            ((uint8_t)(1<<0))
#define RF_ENABLE           ((uint8_t)(1<<1))
#define INT_ENABLE          ((uint8_t)(1<<2))
#define INTO_HIGH           ((uint8_t)(1<<3))
#define INTO_DRIVE          ((uint8_t)(1<<4))
#define BIP_8               ((uint8_t)(1<<5))
#define STANDBY_ENB         ((uint8_t)(1<<6))

/* STATUS_REG bits */
#define READY               ((uint8_t)(1<<0))
#define CRC_ACT             ((uint8_t)(1<<1))
#define RF_BUSY             ((uint8_t)(1<<2))

/* INT_ENABLE_REG bits */
#define EOR_ENB             ((uint8_t)(1<<1))
#define EOW_ENB             ((uint8_t)(1<<2))
#define CRC_CALC_ENB        ((uint8_t)(1<<3))
#define BIP_8_ERR_ENB       ((uint8_t)(1<<4))
#define NDEF_ERR_ENB        ((uint8_t)(1<<5))
#define GENERIC_ERR_ENB     ((uint8_t)(1<<7))

/* INT_FLG_REG bits */
#define EOR_INT_FLG         ((uint8_t)(1<<1))
#define EOW_INT_FLG         ((uint8_t)(1<<2))
#define CRC_CALC_FLG        ((uint8_t)(1<<3))
#define BIP_8_ERR_FLG       ((uint8_t)(1<<4))
#define NDEF_ERR_FLG        ((uint8_t)(1<<5))
#define GENERIC_ERR_FLG     ((uint8_t)(1<<6))

/* WD_REG bits*/
#define WD_ENB              ((uint8_t)(1<<0))
#define WD_TIMEOUT_2S       ((uint8_t)(1<<1))
#define WD_TIMEOUT_32S      ((uint8_t)(1<<2))
#define WD_TIMEOUT_8M30S    ((uint8_t)(1<<3))
#define WD_TIMEOUT_MASK     ((uint8_t)(0x0E))

/* NDEF memory bounds*/
#define RF430_START_ADR     ((uint16_t)(0x0000))
#define RF430_END_ADR       ((uint16_t)(0x0BFF))

typedef struct {
    uint16_t data_addr;
    uint8_t message[0x80];
} MESSAGE_DATA;



#endif /* APPLICATION_DRIVERS_NFC_NFC_H_ */
