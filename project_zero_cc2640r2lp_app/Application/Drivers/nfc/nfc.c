/*
 * nfc.c
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: Y.BAZYK
 */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/BIOS.h>
#include <ti/drivers/PIN.h>
#include <xdc/runtime/Log.h>

#include "Board.h"
#include "Application/Drivers/nfc/nfc.h"
#include "Application/Drivers/I2C_for_nfc/i2c_for_nfc.h"

extern I2C_Handle nfc_i2c_handle;

uint16_t end_of_ndef;
uint16_t cur_of_ndef;
bool active;

void nfc_init(void)
{
    /* reset nfc */
    write_nfc_register(nfc_i2c_handle, CONTROL_REG, SW_RESET);
    Task_sleep(20 * (1000 / Clock_tickPeriod));

    /* enable interrupts */
    write_nfc_register(nfc_i2c_handle, INTERRUPT_ENB_REG, EOR_ENB | EOW_ENB);

    /* enable global interrupts, interrupt output, rf interface */
    write_nfc_register(nfc_i2c_handle, CONTROL_REG, INT_ENABLE | INTO_HIGH | INTO_DRIVE);
}

void nfc_clr_cntr_reg(void)
{
    write_nfc_register(nfc_i2c_handle, CONTROL_REG, 0x00);
}

void nfc_rf_enable(void)
{
    write_nfc_register(nfc_i2c_handle, CONTROL_REG, INT_ENABLE | INTO_HIGH | INTO_DRIVE | RF_ENABLE);
}

void nfc_rf_disable(void)
{
    uint16_t read_reg;

    do {
        read_nfc_register(nfc_i2c_handle, STATUS_REG, (uint8_t *)&read_reg, sizeof(uint16_t));
    } while (!(read_reg & READY));

    write_nfc_register(nfc_i2c_handle, CONTROL_REG, INT_ENABLE | INTO_HIGH | INTO_DRIVE);
}

void nfc_write_data(const MESSAGE_DATA *data)
{
    /* write default ndef */
    write_nfc_data(nfc_i2c_handle, data);
}

